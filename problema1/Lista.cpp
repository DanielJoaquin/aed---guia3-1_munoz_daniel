#include <iostream>
#include "Lista.h"
using namespace std;
/*Constructor*/
Lista::Lista(){}
/*Metodo crear Nodo*/
void Lista::crearNodo(int x){
  Nodo *temp;
  /*Se crea el nodo*/
  temp = new Nodo;
  /*Se le asigna un valor al nodo*/
  temp->numero = x;
  /*Se apuntara a NULL por defecto*/
  temp->sig = NULL;
  /*Si es el primer nodo en la lista, pasa a ser el ultimo tambien*/
  if(this->primero == NULL){
    this->primero = temp;
    this->ultimo = this->primero;
  }
  /*Si no*/
  else{
    /*El nodo apuntara a ultimo*/
    this->ultimo->sig = temp;
    /*Se deja como ultimo nodo al creado*/
    this->ultimo = temp;
  }
}
/*Metodo para ordenar lista de menor a mayor*/
void Lista::ordenar(){
  /*Nodos auxiliares*/
  Nodo *nodo = this->primero;
  Nodo *sucesor = NULL;
  int aux = 0;
  /*Se recorre la lista mientras el sucesor del primero sea NUlo*/
  while((nodo->sig) != NULL){
    sucesor = nodo->sig;
    /*SE recorre la lista mientra el nodo sucesor no sea NULO*/
    while(sucesor != NULL){
      /*Si el numero del primer nodo es mayor que el del sucesor se cambia*/
      if(nodo->numero > sucesor->numero){
        /*Se guarda el numero del nodo sucesor en variable auxiliar*/
        aux = sucesor->numero;
        /*El nodo sucesor toma el valor del numero del primer nodo*/
        sucesor->numero = nodo->numero;
        /*El primero nodo toma el valor de la variable auxiliar*/
        nodo->numero = aux;
      }
      /*Se avanza de nodo para seguir comparando*/
      sucesor = sucesor->sig;
    }
    /*SE avanza de nodo para seguor comparando*/
    nodo = nodo->sig;
  }
}
/*Metodo para imprimir*/
void Lista::imprimir(){
  /*temp toma el valor del primer nodo*/
  Nodo *temp = this->primero;

  cout << "--------------------------------------" << endl;
  /*Se recorre la lista hasta que el nodo actual apunte a NULo*/
  while(temp != NULL){
    cout << "( " << temp->numero << " )" << "-";
    /*temp tomara el valor del nodo al que apunta el nodo actual*/
    temp = temp->sig;
  }
  cout << "\n--------------------------------------" << endl;
}

void Lista::separar(Lista *positivos, Lista *negativos){
  /*temp Recorre la lista que se llama*/
  Nodo *temp = this->primero;
  /*Se recorre la lista hasta que apunte a NUlo*/
  while(temp != NULL){
    /*Si el numero del nodo es mayor a 0, se le agrega a la lista de positivos*/
    if((temp->numero) > 0){
      /*Se llama a la funcion para crear el nodo y este toma el valor del numero*/
      positivos->crearNodo(temp->numero);
    }
    /*Si no, se le agrega al la lista negativos*/
    else{
      /*Se llama a la funcion para crear el nodo y este toma el valor del numero*/
      negativos->crearNodo(temp->numero);
    }
    /*temp apuntara al siguiente nodo*/
    temp = temp->sig;
  }
}
