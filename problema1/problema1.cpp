#include <iostream>
#include "Inicio.h"
using namespace std;

int main(){

  /*Variables*/
  string line;
  int num = 1;

  Inicio inicio = Inicio();
  Inicio iniciop = Inicio();
  Inicio inicion = Inicio();

  Lista *lista = inicio.get_Lista();
  Lista *positivos = iniciop.get_Lista(); /*Lista Valores positivos*/
  Lista *negativos = inicion.get_Lista(); /*Lista Valores negativos*/

  cout << "Ingrese 0 para dejar de agregar numero a la lista" << endl;

  while(num != 0){
    cout << "Ingrese un numero entero: ";
    getline(cin, line);
    num = stoi(line);

    if(num != 0){
      /*Se crea nodo con el numero que ingresa*/
      lista->crearNodo(num);
      /*Se impime la lista*/
      cout << "|ESTADO ACTUAL|" << endl;
      lista->imprimir();
    }
  }

  /*Se imprime la lista*/
  cout << "\n\n|LISTA|" << endl;
  lista->imprimir();
  /*Ordenamiento de la lista*/
  lista->ordenar();

  /*Separador de listas*/
  lista->separar(positivos, negativos);

  /*Impresion de lista n° positivos*/
  cout << "\n\n|LISTA N° POSITIVOS|" << endl;
  positivos->imprimir();
  /*Impresion de lista n° negativos*/
  cout << "\n\n|LISTA N° NEGATIVOS|" << endl;
  negativos->imprimir();

  return 0;
}
