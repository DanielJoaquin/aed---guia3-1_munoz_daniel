#include <iostream>
#include "Lista.h"
using namespace std;

#ifndef INICIO_H
#define INICIO_H

class Inicio{
  private:
    Lista *lista = NULL;

  public:
    /*Constructor*/
    Inicio();
    /*Metodo*/
    Lista *get_Lista();
};
#endif
