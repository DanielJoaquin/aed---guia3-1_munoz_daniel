#include <iostream>
#include "Lista.h"
using namespace std;
/*Constructor*/
Lista::Lista(){}
/*Metodo crear Nodo*/
void Lista::crearNodo(string x){
  Nodo *temp;
  /*Se crea el nodo*/
  temp = new Nodo;
  /*Se le asigna un nombre al nodo*/
  temp->nombre = x;
  /*Se apuntara a NULL por defecto*/
  temp->sig = NULL;
  /*Si es el primer nodo en la lista, pasa a ser el ultimo tambien*/
  if(this->primero == NULL){
    this->primero = temp;
    this->ultimo = this->primero;
  }
  /*Si no*/
  else{
    /*El nodo apuntara a ultimo*/
    this->ultimo->sig = temp;
    /*Se deja como ultimo nodo al creado*/
    this->ultimo = temp;
  }
}
/*Metodo para ordenar lista alfabeticamenter*/
void Lista::ordenar(){
  /*Nodos auxiliares*/
  Nodo *nodo = this->primero;
  Nodo *sucesor = NULL;
  string aux = "\0";
  /*Se recorre la lista mientras el sucesor del primero sea NUlo*/
  while((nodo->sig) != NULL){
    sucesor = nodo->sig;
    /*SE recorre la lista mientra el nodo sucesor no sea NULO*/
    while(sucesor != NULL){
      /*Si el nombre del primer nodo, alfabeticamente, es primero se reordena*/
      /*f(x) .compare, devuelve numero mayor a 0 cuando nodo->nombre va primero que sucesor->nombre*/
      if(nodo->nombre.compare(sucesor->nombre) > 0){
        /*Se guarda el string del nodo sucesor*/
        aux = sucesor->nombre;
        /*El primer nodo sera su sucesor*/
        sucesor->nombre = nodo->nombre;
        /*El primer nodo tomara el string almacenado en variable aux*/
        nodo->nombre = aux;
      }
      /*Se avanza de nodo para seguir comparando*/
      sucesor = sucesor->sig;
    }
    /*SE avanza de nodo para seguor comparando*/
    nodo = nodo->sig;
  }
}
/*Metodo para imprimir*/
void Lista::imprimir(){
  /*temp toma el valor del primer nodo*/
  Nodo *temp = this->primero;

  cout << "--------------------------------------" << endl;
  /*Se recorre la lista hasta que el nodo actual apunte a NULo*/
  while(temp != NULL){
    cout << "| " << temp->nombre << " |" << "-";
    /*temp tomara el valor del nodo al que apunta el nodo actual*/
    temp = temp->sig;
  }
  cout << "\n--------------------------------------" << endl;
}
