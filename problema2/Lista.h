#include <iostream>
using namespace std;

#ifndef LISTA_H
#define LISTA_H

typedef struct Nodo{
  string nombre = "\0";
  struct Nodo *sig;
} Nodo;

class Lista{
  private:
    Nodo *primero = NULL;
    Nodo *ultimo = NULL;
  public:
    /*Constructor*/
    Lista();
    /*Metodos*/
    void crearNodo(string x);
    void ordenar();
    void imprimir();
    

};
#endif
