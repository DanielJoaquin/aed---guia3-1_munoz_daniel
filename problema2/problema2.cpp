#include <iostream>
#include "Inicio.h"
using namespace std;

int main(){

  /*Variables*/
  string line;

  Inicio inicio = Inicio();
  Lista *lista = inicio.get_Lista();

  while(line.compare("x") != 0){
    cout << "Ingrese 'x' para dejar de agregar nombres a la lista" << endl;
    cout << "Ingrese un nombre: ";
    getline(cin, line);

    if(line.compare("x") != 0){
      /*Se crea nodo con el numero que ingresa*/
      lista->crearNodo(line);
      /*Ordenamiento de la lista alfabeticamente*/
      lista->ordenar();
      /*Se impime la lista*/
      cout << "|ESTADO ACTUAL|" << endl;
      lista->imprimir();
    }
  }

  /*Se imprime la lista*/
  cout << "\n\n|LISTA DE NOMBRES|" << endl;
  lista->imprimir();


  return 0;
}
