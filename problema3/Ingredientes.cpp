#include <iostream>
#include "Ingredientes.h"
using namespace std;

/*Constructor*/
Ingredientes::Ingredientes(){}

/*Metodos*/
/*Metodo para verificar si la lista esta vacia*/
bool Ingredientes::listaVacia(){
  if(this->primero == NULL){
    return true;
  }
  else{
    return false;
  }
}
/*Metodo para crear Ingredientes*/
void Ingredientes::crearIngrediente(string nombre){
  NodoIngrediente *temp;
  /*Se crea el nodo*/
  temp = new NodoIngrediente;
  /*Se le asigna un nombre al nodo*/
  temp->ingrediente = nombre;
  /*Se apuntara a NULL por defecto*/
  temp->sig = NULL;
  /*Si es el primer nodo en la lista, pasa a ser el ultimo tambien*/
  if(this->primero == NULL){
    this->primero = temp;
    this->ultimo = this->primero;
  }
  /*Si no*/
  else{
    /*El nodo apuntara a ultimo*/
    this->ultimo->sig = temp;
    /*Se deja como ultimo nodo al creado*/
    this->ultimo = temp;
  }
  cout << "\nINGREDIENTE AGREGADO" << endl;
}

/*Metodo para imprimir*/
void Ingredientes::imprimir(){
  /*temp toma el valor del primer nodo*/
  NodoIngrediente *temp = this->primero;
  /*Contador de ingrediente*/
  int contador = 0;

  cout << "--------------------------------------" << endl;
  /*Se recorre la lista hasta que el nodo actual apunte a NULo*/
  while(temp != NULL){
    cout << "| " << temp->ingrediente << " |" << endl;
    /*temp tomara el valor del nodo al que apunta el nodo actual*/
    contador = contador + 1;
    temp = temp->sig;
  }
  if(contador == 0){
    cout << "Postre sin ingredientes!" << endl;
  }
  cout << "--------------------------------------" << endl;
}
/*Metodo para borrar un ingrediente*/
void Ingredientes::borrar(string nombre){
  /*Si la lista tiene elementos, es posible eliminar*/
  if(this->primero != NULL){
    NodoIngrediente *sucesor = this->primero;
    NodoIngrediente *anterior = NULL;
    /*Mientras el nodo sucesor, que comienza en primero, sea diferente a nulo e ingrediente
    sea diferente del valor de la variable del nodo, se sigue recorriendo la lista*/
    while((sucesor != NULL) && (sucesor->ingrediente.compare(nombre) != 0)){
      /*Se igual los nodos*/
      anterior = sucesor;
      /*EL nodo sucesor avanza a un nodo para continuar comparando*/
      sucesor = sucesor->sig;
    }
    /*si sucesor llega a ser nulo*/
    if(sucesor == NULL){
      cout << "EL INGREDIENTE BUSCADO NO EXISTE" << endl;
    }
    /*Si amterior es nulo significa que el elemento es el primero*/
    if(anterior == NULL){
      /*El primero nodo sera el nodo que apunta y asi se pierde referencia del primero
      y se elimina*/
      this->primero = this->primero->sig;
      cout << "INGREDIENTE ELIMINADO" << endl;
    }
    /*Si no, el elemento buscado esta en otra posicion*/
    else{
      /*nodo anterior al buscado apuntara a lo que apunta el nodo buscado*/
      anterior->sig = sucesor->sig;
      /*nodo buscado apuntara a nulo*/
      sucesor->sig == NULL;
      cout << "INGREDIENTE ELIMINADO" << endl;
    }
  }
  else{
    cout << "NO EXISTEN INGREDIENTES PARA ELIMINAR" << endl;
  }
}
