#include <iostream>
using namespace std;

#ifndef INGREDIENTES_H
#define INGREDIENTES_H

/*Definicion de estructura para ingrediente*/
typedef struct NodoIngrediente{
  string ingrediente = "\0";
  struct NodoIngrediente *sig;
} NodoIngrediente;

class Ingredientes{
  private:
    NodoIngrediente *primero = NULL;
    NodoIngrediente *ultimo = NULL;
  public:
    /*Constructor*/
    Ingredientes();

    /*Metodos*/
    bool listaVacia();
    void crearIngrediente(string nombre);
    void imprimir();
    void borrar(string nombre);
};
#endif
