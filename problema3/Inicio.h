#include <iostream>
#include "Postres.h"
using namespace std;

#ifndef INICIO_H
#define INICIO_H

class Inicio{
  private:
    Postres *postres = NULL;

  public:
    /*Constructor*/
    Inicio();
    /*Metodo*/
    Postres *get_Postres();
};
#endif
