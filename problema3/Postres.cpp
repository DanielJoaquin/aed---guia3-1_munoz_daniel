#include <iostream>
#include "Postres.h"
using namespace std;

/*Constructor*/
Postres::Postres(){}

/*Metodos*/
/*Metodo para ver si la lista esta vacia*/
bool Postres::listaVacia(){
  if(this->primero == NULL){
    return true;
  }
  else{
    return false;
  }
}
/*Metodo para crear un postre*/
void Postres::crearPostre(string nombre){
  NodoPostre *temp;
  /*Se crea el nodo*/
  temp = new NodoPostre;
  /*Se le asigna un nombre al nodo*/
  temp->postre = nombre;
  /*Se apuntara a NULL por defecto*/
  temp->sig = NULL;
  /*Si es el primer nodo en la lista, pasa a ser el ultimo tambien*/
  if(this->primero == NULL){
    this->primero = temp;
    this->ultimo = this->primero;
  }
  /*Si no*/
  else{
    /*El nodo apuntara a ultimo*/
    this->ultimo->sig = temp;
    /*Se deja como ultimo nodo al creado*/
    this->ultimo = temp;
  }
  cout << "\nPOSTRE AGREGADO" << endl;
}
/*Metodo para agregar ingrediente a postre*/
void Postres::agregarIngrediente(string busqueda, string ingredienteNuevo){
  NodoPostre *temp = this->primero;
  /*Se busca el postre al que se quiere agregar el ingrediente*/
  while(temp != NULL){
    /*Si el nombre ingresado por el usuario coincide con el nombre del postre
    se encuentra el postre deseado*/
    if(temp->postre.compare(busqueda) == 0){
      /*Se llama al metodo crearIngrediente*/
      temp->listaIngredientes->crearIngrediente(ingredienteNuevo);
    }
    temp = temp->sig;
  }

}
/*Metodos para ordenar los postres*/
void Postres::ordenar(){
  NodoPostre *primer = this->primero;
  NodoPostre *sucesor = NULL;
  string aux = "\0";

  /*Mientras lo que apunta el sucesor sea diferente de nulo*/
  while(primer->sig != NULL){
    sucesor = primer->sig;
    /*MIentras el nodo sucesor no sea nulo*/
    while(sucesor != NULL){
      /*SI el nombre del primer nodo es primero alfabeticamente, se hace un reordenamiento
      La funcion .compare, devuelve un numero mayor a 0 si primer->nombre va primero que sucesor->nombre*/
      if(primer->postre.compare(sucesor->postre) > 0){
        /*Se guarda el string del nodo sucesor*/
        aux = sucesor->postre;
        /*El primer nodo se convierte en su sucesor*/
        sucesor->postre = primer->postre;
        /*El primer nodo toma el valor de la variable aux*/
        primer->postre = aux;
      }
      sucesor = sucesor->sig;
    }
    primer = primer->sig;
  }
}
/*Metodos para borrar postres*/
void Postres::borrarPostre(string nombre){
  /*Si la lista tiene elementos, es posible eliminar*/
  if(this->primero != NULL){
    NodoPostre *sucesor = this->primero;
    NodoPostre *anterior = NULL;
    /*Mientras el nodo sucesor, que comienza en primero, sea diferente a nulo y postre
    sea diferente del valor de la variable del nodo, se sigue recorriendo la lista*/
    while((sucesor != NULL) && (sucesor->postre.compare(nombre) != 0)){
      /*Se igual los nodos*/
      anterior = sucesor;
      /*EL nodo sucesor avanza a un nodo para continuar comparando*/
      sucesor = sucesor->sig;
    }
    /*si sucesor llega a ser nulo*/
    if(sucesor == NULL){
      cout << "EL POSTRE BUSCADO NO EXISTE" << endl;
    }
    /*Si amterior es nulo significa que el elemento es el primero*/
    if(anterior == NULL){
      /*El primero nodo sera el nodo que apunta y asi se pierde referencia del primero
      y se elimina*/
      this->primero = this->primero->sig;
      cout << "POSTRE ELIMINADO" << endl;
    }
    /*Si no, el elemento buscado esta en otra posicion*/
    else{
      /*nodo anterior al buscado apuntara a lo que apunta el nodo buscado*/
      anterior->sig = sucesor->sig;
      /*nodo buscado apuntara a nulo*/
      sucesor->sig == NULL;
      cout << "POSTRE ELIMINADO" << endl;
    }
  }
  else{
    cout << "NO EXISTEN POSTRES PARA ELIMINAR" << endl;
  }
}
/*Metodos para imprimir postres*/
void Postres::imprimir(){
  /*temp toma el valor del primer nodo*/
  NodoPostre *temp = this->primero;
  /*Contador de Postre*/
  int contador = 0;

  cout << "\n--------------------------------------" << endl;
  cout << "            |POSTRES|" << endl;
  cout << "--------------------------------------" << endl;
  /*Se recorre la lista hasta que el nodo actual apunte a NULo*/
  while(temp != NULL){
    cout << "| " << temp->postre << " |" << endl;
    /*temp tomara el valor del nodo al que apunta el nodo actual*/
    contador = contador + 1;
    temp = temp->sig;
  }
  if(contador == 0){
    cout << "NO HAY POSTRES!" << endl;
  }
  cout << "--------------------------------------" << endl;
}
/*Metodos para los ingredientes de los postres*/
void Postres::mostrarIngredientes(string busqueda){
  NodoPostre *temp = this->primero;

  /*Se recorre la lista de postres para hacer busqueda del postre deseado*/
  while(temp != NULL){
    /*Se compara el nombre del postre con los de la lista*/
    if(temp->postre.compare(busqueda) == 0){
      /*Se imprime su nombre y sus ingredientes*/
      cout << "--------------------------------------" << endl;
      cout << "\n\nPostre: " << temp->postre << endl;
      /*Se imprimen los ingredientes*/
      temp->listaIngredientes->imprimir();
    }
    temp = temp->sig;
  }
}
/*Metodos para eliminar ingrediente de un postre*/
void Postres::eliminarIngrediente(string busqueda, string nombreIngrediente){
  NodoPostre *temp = this->primero;

  /*Se recorre la lista para buscar el postre*/
  while(temp != NULL){
    /*SI el nombre del postre buscado esta en la lista*/
    if(temp->postre.compare(busqueda) == 0){
      /*SE llama al metodo para eliminar un ingrediente de ese postre*/
      temp->listaIngredientes->borrar(nombreIngrediente);
    }
    temp = temp->sig;
  }
}
