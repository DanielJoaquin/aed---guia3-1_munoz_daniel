#include <iostream>
#include "Ingredientes.h"
using namespace std;

#ifndef POSTRES_H
#define POSTRES_H

/*Definicion de estructura para ingrediente*/
typedef struct NodoPostre{
  string postre = "\0";
  Ingredientes *listaIngredientes = new Ingredientes();
  struct NodoPostre *sig;
} NodoPostre;

class Postres{
  private:
    NodoPostre *primero = NULL;
    NodoPostre *ultimo = NULL;
  public:
    /*Constructor*/
    Postres();

    /*Metodos*/
    bool listaVacia();
    void crearPostre(string nombre);
    void agregarIngrediente(string busqueda, string ingredienteNuevo);
    void ordenar();
    void borrarPostre(string nombre);
    void imprimir();
    void mostrarIngredientes(string busqueda);
    void eliminarIngrediente(string busqueda, string nombreIngrediente);
};
#endif
