#include <iostream>
#include "Inicio.h"
using namespace std;

/*Menu del programa*/
int Menu(){
  string opcion;
  cout << "\n--------Menu Principal--------" << endl;
  cout << "|[1] MOstrar Listado POstres |" << endl;
  cout << "|[2] Agregar Postres         |" << endl;
  cout << "|[3] Eliminar Postra         |" << endl;
  cout << "|[4] Seleccionar Postre      |" << endl;
  cout << "|[5] Salir                   |" << endl;
  cout << "------------------------------" << endl;
  cout << "\nIngrese Opcion: ";
  getline(cin, opcion);

  /*Validacion si la opcion es correcta*/
  while ((stoi(opcion) > 5) || (stoi(opcion) < 1)){
    cout << "\nOPCION NO VALIDA\nIngrese Opcion:";
    getline(cin, opcion);
  }

  return stoi(opcion);
}

/*Menu de la opcion4*/
int Menu2(){
  string opcion;
  cout << "\n-------Menu de POstres--------" << endl;
  cout << "|[1] MOstrar ingredientes    |" << endl;
  cout << "|[2] Agregar Ingrediente     |" << endl;
  cout << "|[3] Eliminar INgrediente    |" << endl;
  cout << "|[4] Volver al menu Principal|" << endl;
  cout << "------------------------------" << endl;
  cout << "\nIngrese Opcion: ";
  getline(cin, opcion);

  /*Validacion si la opcion es correcta*/
  while ((stoi(opcion) > 4) || (stoi(opcion) < 1)){
    cout << "\nOPCION NO VALIDA\nIngrese Opcion:";
    getline(cin, opcion);
  }

  return stoi(opcion);
}

/*Funcion para ingresar el nombre de un ingrediente o nombre*/
string ingresar(){
  string line;
  getline(cin, line);
  return line;
}

int main(){

  /*Variables*/
  string line;
  string NomIngrediente = "\0";
  int opcion = 0;

  Inicio inicio = Inicio();
  Postres *listaPostres= inicio.get_Postres();

  while(opcion != 5){

    opcion = Menu();

    switch(opcion) {
      case 1: /*Mostrar listado de postres*/
        listaPostres->imprimir();
        break;
      case 2: /*Agregar Postre*/
        /*LLamado a la funcion ingresar*/
        cout << "Nombre Postre: ";
        line = ingresar();
        /*Se envia como parametro a la funcion para crear Postre*/
        listaPostres->crearPostre(line);
        /*Se ordena la lista de Postres*/
        listaPostres->ordenar();
        break;
      case 3: /*Eliminar postre*/
        /*Si la lista no esta vacia, es posible eliminar un postre*/
        if(listaPostres->listaVacia() == false){
          listaPostres->imprimir();
          /*INgreso de nombre a eliminar*/
          cout << "Nombre del postre que desa eliminar: ";
          line = ingresar();
          listaPostres->borrarPostre(line);
        }
        else{
          cout << "NO EXISTEN POSTRES PARA ELIMINAR" << endl;
        }
        break;
      case 4: /*Seleccion de postre*/
        opcion = 0;
        /*El menu 2 funcionara solo si la opcion es diferente de 4*/
        while(opcion != 4){
          /*Llamado a la funcion menu para la opcion 4*/
          opcion = Menu2();
          listaPostres->imprimir();

          switch (opcion) {
            case 1: /*Mostrar ingredientes de postre deseado*/
              /*Ingreso de postre*/
              cout << "NOmbre Postre: ";
              line = ingresar();
              /*LLamado al metodo para mostrar los ingredientes y se enviara como
              parametro el nombre del postre*/
              listaPostres->mostrarIngredientes(line);
              break;
            case 2: /*Agregar ingrediente al postre*/
              /*Ingreso de postre*/
              cout << "NOmbre Postre: ";
              line = ingresar();
              /*Ingreso de ingredientes*/
              while(NomIngrediente.compare("x") != 0){
                cout << "Ingrese 'x' para salir" << endl;
                cout << "Ingrediente: ";
                NomIngrediente = ingresar();
                /*Se enviaran parametros para agregar ingrediente al postre deseado*/
                if(NomIngrediente.compare("x") != 0){
                  listaPostres->agregarIngrediente(line, NomIngrediente);
                }
              }
              break;
            case 3: /*Eliminar ingrediente*/
              /*Ingreso de postre*/
              cout << "NOmbre Postre: ";
              line = ingresar();
              /*INgreso nombre del ingrediente a eliminar*/
              cout << "Ingrediente a eliminar: ";
              NomIngrediente = ingresar();
              listaPostres->eliminarIngrediente(line, NomIngrediente);
              break;
            case 4:
              break;
          }
        }
        break;

    }
  }

  return 0;
}
